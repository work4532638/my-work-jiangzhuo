#

本库学习分享用

[网址主页](https://gitlab.com/work4532638/my-work-jiangzhuo/-/blob/master/README.md)

## 民间借贷案件审判中重点及难点

2023年9月14日 下午14：30 - 17：00 四楼会议室

最高院民一庭 XXX

1. 原版
    1. [文字整理](https://gitlab.com/work4532638/my-work-jiangzhuo/-/blob/master/2023.09.14民间借贷案件审判中重点及难点/文字整理.md)
    1. [发帖地址（可能需要注册账号）](https://gitee.com/wyl350/my-work-jiangzuo/issues/I823LS)

-------

## 审理工伤保险行政案件的重点难点问题

1. [文字内容](https://gitlab.com/work4532638/my-work-jiangzhuo/-/blob/master/2023.09.19审理工伤保险行政案件的重点难点问题/index.md)
1. [发帖地址（可能需要注册账号）](https://gitee.com/wyl350/my-work-jiangzuo/issues/I82RKO)

## 2023.09.20家事审判中的重点难点问题

1. [文字内容](https://gitlab.com/work4532638/my-work-jiangzhuo/-/blob/master/2023.09.20家事审判中的重点难点问题/index.md)
1. [发帖地址（可能需要注册账号）](https://gitee.com/wyl350/my-work-jiangzuo/issues/I831D5)

