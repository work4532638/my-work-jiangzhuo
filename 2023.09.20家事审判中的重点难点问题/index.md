## 2023.09.20家事审判中的重点难点问题

最高院 民一庭 王丹

未摘，补充？？？？？？？？
1. 16年，《反家暴法》
1. 婚姻法编 
1. 婚姻法编解释

一、关于反家庭暴力的问题

指导思想:坚持以习近平新时代中国特色社会主义思想为指导，深入贯彻习近平法治思想和习近平总书记关于注重家庭家教家风的重要论述精神，在家庭中积极培育和践行社会主义核心价值观，涵养优良家风，弘扬家庭美德，最大限度预防和制止家庭暴力。

民法典关于家庭暴力的规定，除1042条倡导性规定外，1079和1091是关于离婚和离婚损害赔偿的规定

实践中注意家庭暴力认定与人身安全保护令证据的认定标准的不同。

适当加大依职权调取证据力度，尤其是精神暴力、性暴力等方面。

司法解释表述上，改变了原来定义式的表述方式，更具有开放性。

只要认定是暴力的，一定是要判离婚的。

婚姻法第3条(民法典1042条):禁止家庭暴力。禁止家庭成员间的虐待和遗弃。

婚姻法第32条(民法典1079条):有下列情形之一，调解无效的，应当准予离婚:(二)实施家庭暴力或者虐待、遗弃家庭成员。

婚姻法第41条(民法典1091条):有下列情形之一，导致离婚的，无过错方有权请求损害赔偿:(三)实施家庭暴力;(四)虐待、遗弃家庭成员

二、同居关系问题

1.有配偶者与他人同居问题

民法典婚姻家庭编司法解释一第二条:与他人同居，是指有配偶者与婚外异性，不以夫妻名义，持续、稳定地共同居住。

与婚外同性同居问题在实践中如何适用?

法律的表述为“与他人同居”. 

2.起诉解除同居关系的，不予受理，不再设例外规定“有配偶者与他人同居”

3.同居析产纠纷处理原则

原司法解释-第15条，表述有修改，以强调这一理念:如果有证据证明为一方所有的，即首先认定为个人财产，以区别于合法的婚姻关系;要注意本条的适用条件是被宣告无效或者被撤销婚姻的情况。不同于一般意义上的同居。

(1054条，原婚姻法12条)同居期间所得的财产，由当事人协议处理;协议不成时，由人民法院根据照顾无过错方的原则判决。

1. 照顾未成年人，非婚生子女的待遇与婚生子女一样
1. 不一定有照顾妇女的原则（婚姻编中 规定有 照顾保护老人残疾人妇女未成年人。)
1. 同居长短对于析产，考虑财产的贡献程度。

三、彩礼问题

第一项双方未办理结婚登记的，需要限定为且未共同生活的条件其他情况下彩礼返还问题

(1)彩礼与一般赠与的区别，需要以彩礼的习俗性为基础，考量给付时间、方式、金额等。(已习惯为法源基础)

(2)当事人地位列明。

(3)未办理结婚登记的，原则上应当返还，但共同生活或已经生育子女的，可酌减;

(4)已经办理结婚登记，原则上不予返还。但对于高价的部分，可考虑根据双方共同生活的时间、彩礼数额、彩礼用途、是否生育子女，并结合当地风俗习惯等因素，酌情确定是否返还以及返还的具体数额。

学理观点： 
1. 附条件的赠与
附带解除条件的赠与，婚姻不成的时候，要求返还。这是全有或者全无的情况。
1. 一婚姻为目的的赠与,台湾偏向此观点。
    1. 登记
    1. 共同生活
    1. 生育子女

四、民法典婚姻家庭编解释四个条文中涉及一年期间的规定均予以删除

(1)解释二第五条 夫妻一方或者双方死亡后一年内，生存一方或者利害关系人依据婚姻法第十条的规定申请宣告婚姻无效的，人民法院应当受理。

(2)解释二第九条 男女双方协议离婚后一年内就财产分割问题反悔，请求变更或者撤销财产分割协议的，人民法院应当受理。

(3)第三十条 人民法院受理离婚案件时，应当将婚姻法第四十六条等规定中当事人的有关权利义务，书面告知当事人。在适用婚姻法第四十六条时，应当区分以下不同情况:

(一)符合婚姻法第四十六条规定的无过错方作为原告基于该条规定向人民法院提起损害赔偿请求的，必须在离婚诉讼的同时提出。

(二)符合婚姻法第四十六条规定的无过错方作为被告的离婚诉讼案件，如果被告不同意离婚也不基于该条规定提起损害赔偿请求的，可以在离婚后一年内就此单独提起诉讼。

(三)无过错方作为被告的离婚诉讼案件，一审时被告未基于婚姻法第四十六条规定提出损害赔偿请求，二审期间提出的，人民法院应当进行调解，调解不成的，告知当事人在离婚后一年内另行起诉。(双方当事人同意由第二审人民法院一并审理的，第二审人民法院可以一并裁判)

(4) 解释二第二十七条当事人在婚姻登记机关办理离婚登记手续后，以婚姻法第四十六条规定为由向人民法院提出损害赔偿请求的，人民法院应当受理。但当事人在协议离婚时已经明确表示放弃该项请求，或者在办理离婚登记手续一年后提出的，(人民法院)不予支持。

五、因受胁迫或非法限制人身自由撤销婚姻中，撤销权的行使期间问题

撤销权属于形成权，适用除斥期间的规定，不适用诉讼时效中止、中断、一 延长的规定。

基于总则编152条第二款增加撤销权行使期间客观标准的规定，司法解释在原解释一第12条基础上增加第二款，明确不适用总则编152条。

一方通过冒名顶替或者伪造证件等方式违规办理结婚登记，另一方未亲自到婚姻登记机关申请结婚登记。根据民法典第1049条规定，要求结婚的男女双方应当亲自到婚姻登记机关申请结婚登记。因女方并未表达结婚意愿，婚姻应当认定为不成立。对于已经办理的结婚登记，因登记行为存在严重瑕疵，可以通过行政诉讼撤销或确认无效。实践中，该类案件往往因为时间久远超过行政诉讼法规定的最长起诉期限，为此，根据2021年四部委《关于妥善处理以冒名顶替或者弄虚作假的方式办理婚姻登记问题的指导意见》规定，人民法院应当首先向民政部门发送撒销婚姻登记的司法建议书，由民政部门审核后撤销结婚登记，对当事人一方提起的离婚诉讼，人民法院裁定驳回起诉。

六、抢夺、藏匿未成年子女问题

具体有以下两个方面的问题:一是双方未提起离婚诉讼，在分居期间一方将未成年子女抢夺、藏匿，致使另一方无法直接行使教育、保护权利。我们经研究认为，教育、保护未成年人子女不仅是父母的义务，也是一种权利，是亲权的重要内容之一，一方擅自抢夺、藏匿未成年子女的，侵害了另一方的监护权，**可以监护权纠纷作为案由。**监护权纠纷是指因行使监护权而发生的民事争议，主要是监护权人认为其依法行使的监护权被他人侵害时所引发的纠纷。

同时，根据民法典第997条规定，另一方也可以申请人格权行为禁令。人民法院可以签发禁令保障另一方的探视权。二是离婚诉讼中直接抚养人的确定。从我们了解的情况看，大部分抢夺、藏匿的未成年子女均小于8周岁，很多孩子因长期见不到父母一方，已经不认识或者不熟悉。实践中，为了不制造更多矛盾，很多法院倾向干认定未成年子女已经形成了稳定的生活关系，反倒倾向于判决实际抚养一方获得直接抚养权。但该类判决却也广受诟病。我们经研究认为，在离婚纠纷确定子女归哪一方时，不宜绝对因一方抢夺、藏匿未成年子女为由即否认其直接抚养权，还要考虑原有司法解释规定的各类因素情况，但可以进一步规范，将此类情形作为优先考虑将直接抚养权给另一方的情形。

七、父母为子女出资购房问题

对解释二第22条进行修改，同时删除解释三第7条。

1.父母起诉民间借货纠纷的处理原则。

首先要明确一个前提就是现有证据能够证明是赠与的情况下，司法解释主要解决赠与一方还是双方的问题，如果有证据能够证明是借款关系，则应当认定是借款关系

2.赠与一方还是赠与双方认定问题

婚前父母为子女购置房屋出资的，认定为是子女个人的赠与

婚后的父母为子女购置房屋出资的，引导各方通过协议明确约定。没有约定或者约定不明的，按照民法典1062条第一款第四项确定的原则处理。

婚姻法和民法典确定的基本原则:婚姻关系存续期间，受赠的财产原则上归夫妻共同所有，除非赠与合同中确定只归一方的财产

八、离婚协议约定夫妻将共同财产中的房产或一方个人房产过户到子女名下，离婚后，不动产登记一方能否单方主张撤销该项约定?子女是否有独立的请求权?

我们经研究认为，此种情况下，一方不享有任意撤销权，理由是:1.离婚时夫妻共同财产由双方协议处理是前提。双方在离婚协议中约定的共同所有的房产归子女实质上即是夫妻双方对共同财产的处理协议，该协议对双方具有法律约束力，不能任何撤销。2.离婚协议是夫妻双方权衡利益，考量利弊之后，围绕婚姻关系解除而形成的一个有机整体，各项内容既相互独立，又相互依存，其中一项财产分割内容的变化，很可能引起其他财产分割、子女抚养问题的变化，甚至对于是否同意离婚也可能出现不同的结果。离婚协议并非简单的无任何条件的无偿赠与合同。此外，从当事人上看，赠与合同需要赠与人与受赠人之间达成赠与合意，而离婚协议并没有子女的参与。因此，即使赠与财产的权利尚未转移，对离娇协议中涉及的财产赠与，单方并不能任意撤销，除非双方达成新的财产处理协议。

对于子女能否作为原告起诉要求履行协议的问题。我们经研究认为一般来讲不宜赋予子女独立的给付请求权，不能作为原告起诉要求履行合同。主要考虑是，就离婚当事人而言很难定性为债权人和债务人，尤其是离婚协议中约定对第三人赠与条款，并不是夫妻一方向夫妻另一方为给付，自不存在第三人作为辅助人代夫妻另一方接受履行的问题，因此不符合“第三人代债权人接受履行”的法律特征。该约定也不符合“利他合同”的特征，因为基于利他合同第三人独立请求权的存在，一旦第三人明确表示接受其利益，合同当事人则不能随意-变更或撤销第三人依据合同享有的权利，此与实践中允许夫妻离婚后协商变更离婚协议的做法相悖。民法典合同编虽然新增加了第三人利益合同规定，但根据人大法工委解释，在约定为第三人利益的情况下，必须合同中明确约定第三人有请求权，否则不能适用该规定。而夫妻离婚协议中一般不会明确赋予子女以独立请求权。

九、婚前或者婚内赠与问题

民法典464条第二款，婚姻、收养、监护等有关身份关系的协议，适用有关该身份关系的法律规定;没有规定的，可以根据其性质参照适用合同编规定。

在解释三第6条“一方所有的房产赠与另一方” 基础上，增加“共有”(加名)的情形，统一适用赠与合同的有关规定

夫妻间给予一般具有以下三个特征:(1)此种给予客观上无偿;(2)给予人的主观目的是为了实现、安排、维护及保障与对方的共同婚姻生活;(3)给予人设想或期待婚姻共同生活将会持续，而在这个共同体内部其将能够继续分享财产价值及其孳息。对该交易就不能简单套用合同法中的赠与规则。因为赠与人任意撤销权的存在，“纯粹是基于赠与之无偿性”。而夫妻间的房产无偿给于，其潜在对价是受给予方对家庭过往和将来的持续付出。**夫妻一方之一房产无偿给于另一方的约定不能一律是赠与，要根据具体案情综合判断。考虑夫妻间给予的目的，对给予方和受领方予以均街保护**，同时弘扬婚姻家庭领域的诚信原则以及伦理共同体性质，让婚姻中的利他性因素有发挥空间的制度基础。

十、离婚协议约定一方不负担抚养费的处理

(婚姻法司法解释(一))第五十二条父母双方可以协议由一方直接抚养子女并由直接抚养方负担子女全部抚养费。但是，直接抚养方的抚养能力明显不能保障子女所需费用，影响子女健康成长的，人民法院不予支持。

第五十八条 具有下列情形之一，子女要求有负担能力的父或者母增加抚养费的，人民法院应予支持:

(一)原定抚养费数额不足以维持当地实际生活水平;

(二)因子女患病、上学，实际需要已超过原定数额;

(三)有其他正当理由应当增加。
